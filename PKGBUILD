# Maintainer: Philip Müller <philm@manjaro.org>
# Contributor: Evangelos Foutras <evangelos@foutrelis.com>
# Contributor: Xavier Devlamynck <magicrhesus@ouranos.be>

pkgname=libxfce4ui-nocsd
pkgver=4.17.0
pkgrel=1
pkgdesc="Widgets library for the Xfce desktop environment (No CSD fork)"
arch=('x86_64' 'aarch64')
url="https://www.xfce.org/"
license=('GPL2')
provides=("libxfce4ui=$pkgver-$pkgrel")
conflicts=(libxfce4ui)
depends=('libxfce4util' 'gtk3' 'xfconf' 'libsm' 'startup-notification'
         'libgtop' 'libepoxy' 'hicolor-icon-theme')
makedepends=('intltool' 'gobject-introspection' 'vala')
source=(https://github.com/Xfce-Classic/libxfce4ui-nocsd/releases/download/4.17.0/libxfce4ui-4.17.0.tar.bz2)
sha256sums=('ca0095d41b0c02d768191e1a1db68e721e5afb48f29ff299e06ad53bb5f5ecfa')

prepare() {
#  mv "$srcdir/libxfce4ui-4.17.0" "$srcdir/libxfce4ui-$pkgver"
  cd "$srcdir/libxfce4ui-$pkgver"

  local src
  for src in "${source[@]}"; do
    src="${src%%::*}"
    src="${src##*/}"
    [[ $src = *.patch ]] || continue
    echo "Applying patch $src..."
    patch -Np1 < "../$src"
  done
}

build() {
  cd "$srcdir/libxfce4ui-$pkgver"

  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --localstatedir=/var \
    --disable-debug \
    --with-vendor-info='Manjaro Linux'
  make
}

package() {
  cd "$srcdir/libxfce4ui-$pkgver"
  make DESTDIR="$pkgdir" install
}

# vim:set ts=2 sw=2 et:
